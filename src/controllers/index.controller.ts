import { NextFunction, Request, Response } from 'express';
import * as XLSX from "xlsx";
import * as path from 'path';
import fs, { ReadStream } from 'fs';
/** Read excel file using stream  
 * Return a result as array
*/
function readStreamExcel(stream: ReadStream, cb): void {
  var buffers = [];
  stream.on('data', function (data) { buffers.push(data); });
  stream.on('end', function () {
    let buffer = Buffer.concat(buffers);
    const workbook = XLSX.read(buffer, {
      type: "buffer",
      WTF: true
    });
    const sheetNames = workbook.SheetNames;
    const worksheet = workbook.Sheets[sheetNames[0]];
    const result = XLSX.utils.sheet_to_json(worksheet);
    // const columnsArray = XLSX.utils.sheet_to_json(worksheet, { header: 1 })[0];
    cb(result);
  });
}
class IndexController {
  public index = (req: Request, res: Response, next: NextFunction): void => {
    try {
      const impFilePath = path.join(__dirname, `../uploads/damco.xlsx`);
      /** Read excel file */
      const readable = fs.createReadStream(impFilePath);
      readStreamExcel(readable, async (result) => {
        try {
          /** Get result from excel file
           * And modify these result another array
           */
          let resArr: Array<any> = [];
          for await (const obj of result) {
            resArr.push({ state: obj.CNTYNAME, label: obj.OPRNAME, revenue: obj.PLNGENAN || 0, lat: obj.LAT, lng: obj.LON })
          }
          /** Sort the result based on revenue (high to lower) */
          resArr = resArr.sort((n1, n2) => {
            if (n1.revenue > n2.revenue) {
              return -1;
            }
            if (n1.revenue < n2.revenue) {
              return 1;
            }
            return 0;
          })
          let finalArr: Array<any> = [];
          let filterArr: Array<any> = [];
          let finalObj: Object = {};
          /** Get unique state and
           * Getting 5 highest company based on revenue in each state
           */
          for (const item of resArr) {
            if (finalObj[item.state]) {
              if (finalObj[item.state] < 5) {
                finalArr.push(item);
              }
              finalObj[item.state]++;
            }
            else {
              finalObj[item.state] = 1;
              finalArr.push(item);
            }
            if (!filterArr.includes(item.state)) {
              filterArr.push(item.state);
            }
          }
          res.status(200).json({ data: finalArr, filterArr: filterArr, message: 'success' });
        } catch (error) {
          next(error);
        }
      });

    } catch (error) {
      next(error);
    }
  };
}

export default IndexController;
