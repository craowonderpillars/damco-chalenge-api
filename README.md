TypeScript Node Starter

# Pre-reqs
To build and run this app locally you will need a few things:

Install Node.js
Install VS Code

# Getting started
TypeScript + Node

Getting TypeScript
TypeScript itself is simple to add to any project with npm.

npm install -D typescript

# Runnig the project

Use the below command

npm start //// runs for development
npm run build   // runs full build including ESLint
npm run lint    // runs only ESLint