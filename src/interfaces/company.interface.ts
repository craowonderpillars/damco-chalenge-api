export interface Company {
  id: number;
  state: string;
  label: string;
  revenue: number;
  lat: number;
  lng: number;
}
